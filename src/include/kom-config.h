/*
 * $Id: kom-config.h,v 1.12 2003/08/23 16:38:21 ceder Exp $
 * Copyright (C) 1991-1994, 1998-1999, 2002-2003  Lysator Academic Computer Association.
 *
 * This file is part of the LysKOM server.
 * 
 * LysKOM is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by 
 * the Free Software Foundation; either version 1, or (at your option) 
 * any later version.
 * 
 * LysKOM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with LysKOM; see the file COPYING.  If not, write to
 * Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
 * or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
 * MA 02139, USA.
 *
 * Please report bugs at http://bugzilla.lysator.liu.se/. 
 */
/*
 *  config.h
 *
 *  Configuration parameters for compiling.  Contains among other
 *  things all the stupid limits you really want to be without, all the
 *  smart limits that has to be there to make it impossible to crash
 *  the server, and lots of constants and configuration options.
 *
 *  Almost all of this can currently be set in a configuration file.
 *  There is normally no reason to change anything in this file.
 */

/* Collating sequence used. */
#define DEFAULT_COLLAT_TAB	swedish_collate_tab



/* The file kom_perror prints to.  */
/* Yes! This should be stdout. At least during testing and debugging! /ceder */

#define kom_errout	stdout

extern const char *get_default_config_file_name(void);
extern void free_default_config_file_name(void);

/* Communications */

/*
 * MAX_NO_OF_CONNECTIONS must be small enough. Each connection takes one
 * file descriptor, and it is important that there are a few descriptors
 * left so that it is possible to save the data base. lyskomd might crash
 * if this number is too big and that many connection attempts are made
 * simultaneously.
 *
 * The following descriptors are open by LysKOM:
 *   stdin, stdout, stderr   (stdin and stdout could probably be closed.
 *			      The log() function prints to stderr.)
 *   TEXTFILE_NAME	     (always open)
 *   DATAFILE_NAME	     (often open)
 *   STATISTIC_NAME	     (open after a SIGUSR1)
 *   One TCP/IP ports	     (listening for normal connections)
 *   One UDP/IP port	     (probably from -lresolv, but I'm not sure)
 *   One TCP/IP port	     (if HAVE_LIBAUTHUSER is defined)
 *   One TCP and one UDP port (used by ADNS)
 *   Two pipes		     (used by signal.c in liboop)
 * Thus, the max number of connections is the number of available file
 * descriptors minus 13.  This has not been fully tested for a long
 * while, so we subtract 9 more just to be on the safe side.
 *
 * Machines where less or more files are open should set this in the 
 * configure script.
 *
 * Also defined in src/server/testsuite/config/unix.exp.
 */

#ifndef PROTECTED_FDS
#define PROTECTED_FDS (13 + 9)
#endif

#if (defined(HAVE_SETRLIMIT) && defined(RLIMIT_NOFILE) \
        && !defined(HAVE_BROKEN_NOFILE))
#  define USING_RLIMIT_NOFILE 1
#else
#  undef USING_RLIMIT_NOFILE
#endif

/*
 * An upper limit of how many file descriptors lyskomd will have opened
 * simultaneously. This is set at the beginning of main().  The first
 * PROTECTED_FDS file descriptors are never used for clients.
 */
extern int fd_ceiling;

/* What is whitespace? */
extern const char *WHITESPACE;
