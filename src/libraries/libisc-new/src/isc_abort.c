/*
** isc_abort.c                                  Route to handle fatal errors
**
** Copyright (C) 1991, 1998-1999 by Peter Eriksson and
** Per Cederqvist of the Lysator Academic Computer Association.
**
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Library General Public
** License as published by the Free Software Foundation; either
** version 2 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
** Library General Public License for more details.
**
** You should have received a copy of the GNU Library General Public
** License along with this library; if not, write to the Free
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
**
** history:
** 910305 pen      moved into separate file
** (See ChangeLog for recent history)
*/

#include <stdio.h>
#ifdef HAVE_STDLIB_H
#  include <stdlib.h>
#endif
#ifdef HAVE_STDDEF_H
#  include <stddef.h>
#endif
#ifdef HAVE_STDARG_H
#  include <stdarg.h>
#endif
#include <sys/types.h>
#include <sys/socket.h>
#include <time.h>

#include "oop.h"
#include "adns.h"
#include "oop-adns.h"

#include "s-string.h"

#include "isc.h"
#include "intern.h"

static void (*isc_abortfn)(const char *msg) = NULL;

  
#if 0
void
isc_setabortfn(void (*abortfn)(const char *msg))
{
  isc_abortfn = abortfn;
}
#endif


void
isc_abort(const char *message)
{
  if (isc_abortfn)
    (*isc_abortfn)(message);
  else
  {
    fprintf(stderr, "\n\r*** ISC SUBSYSTEM FATAL ERROR: %s\n\r", message);
    abort();
  }
}
  
