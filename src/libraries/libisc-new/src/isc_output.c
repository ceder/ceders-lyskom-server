/*
** isc_output.c                                   Isc data output functions
**
** Copyright (C) 1991, 1998-1999 by Peter Eriksson and
** Per Cederqvist of the Lysator Academic Computer Association.
**
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Library General Public
** License as published by the Free Software Foundation; either
** version 2 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
** Library General Public License for more details.
**
** You should have received a copy of the GNU Library General Public
** License along with this library; if not, write to the Free
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
**
** history:
** 910305 pen      moved into separate file
** 910310 pen      added isc_send()
** (See ChangeLog for recent history)
*/

#ifdef HAVE_STDDEF_H
#  include <stddef.h>
#endif
#ifdef HAVE_STDARG_H
#  include <stdarg.h>
#endif
#include <errno.h>
#ifdef HAVE_STRING_H
#  include <string.h>
#endif
#include <sys/file.h>
#include <errno.h>
#ifndef NULL
#  include <stdio.h>
#endif
#include <sys/types.h>
#include <sys/socket.h>
#include <time.h>
#include <assert.h>

#include "oop.h"
#include "adns.h"
#include "oop-adns.h"

#include "s-string.h"

#include "isc.h"
#include "intern.h"



void isc_flush(struct isc_scb *scb)
{
  struct isc_scb_internal *sci = (struct isc_scb_internal*)scb;
  int size;

  switch (sci->state)
  {
    case ISC_STATE_CLOSING:
      if (sci->wr_msg_q)
      {
	size = isc_killqueue(sci->wr_msg_q);
	if (sci->pub.master->write_change_cb)
	    sci->pub.master->write_change_cb(-size);
	sci->wr_msg_q = NULL;
      }
      sci->sendindex = 0;
      return;

    case ISC_STATE_RUNNING:
    case ISC_STATE_DISABLED:
      isc_oflush(sci);
      return;

    default:
      return;
  }
}

static void *
write_cb(oop_source *src,
	 int fd,
	 oop_event event,
	 void *user)
{
  struct isc_scb_internal *scb = user;
  assert(scb->pub.fd == fd);
  assert(scb->pub.master->event_source == src);
  assert(event == OOP_WRITE);

  isc_oflush(scb);
  return OOP_CONTINUE;
}


void
isc_oflush(struct isc_scb_internal *scb)
{
  struct isc_msg  * msg;
  int           failed;
  int           any_written;
  int           wlen;
  int           loopcnt;
  oop_source   *src;
  
  assert(scb->state != ISC_STATE_LISTENING);

  if (scb->state == ISC_STATE_CLOSING)
      return;

  /* Data in the block buffer? */
  if (scb->sendindex > 0)
  {
    msg = isc_allocmsg(scb->sendindex);
    memcpy(msg->buffer, scb->sendbuf, scb->sendindex);
    msg->length = scb->sendindex;
    if (scb->pub.master->write_change_cb)
      scb->pub.master->write_change_cb(msg->length);
    isc_pushqueue(scb->wr_msg_q, msg);
    scb->sendindex = 0;
  }

  if (scb->pub.fd == -1)
    return;
  
  /* Queued entries? Send as much as possible */
  failed = 0;
  loopcnt = 0;
  any_written = 0;
  while ((msg = isc_topqueue(scb->wr_msg_q)) != NULL &&
	 !failed && loopcnt < scb->cfg->max.dequeuelen)
  {
    wlen = write(scb->pub.fd, msg->buffer, msg->length);

    if (wlen < 0)
    {
      if (errno == EWOULDBLOCK)
	wlen = 0;
      else
      {
	scb->write_err_cb(&scb->pub, errno);
	scb->state = ISC_STATE_CLOSING;
	scb->sendindex = 0;
	return;
      }
    }
    else
    {
      any_written = 1;
      scb->wr_msg_q->bytes -= wlen;
      if (scb->pub.master->write_change_cb)
	scb->pub.master->write_change_cb(-wlen);
    }

    msg->length -= wlen;
    if (msg->length > 0)
    {
      /* The write handler could not transmit the whole buffer */
      failed = 1;
      if (wlen > 0)
	memmove(msg->buffer, msg->buffer + wlen, msg->length);
    }
    else
    {
      /* Drop the topmost entry */
      msg = isc_popqueue(scb->wr_msg_q);
      isc_freemsg(msg);
    }

    loopcnt++;
  }

  isc_check_read_callback(scb, any_written);

  if (isc_pollqueue(scb->wr_msg_q))
  {
    if (!scb->write_cb_registered)
    {
      src = scb->pub.master->event_source;
      src->on_fd(src, scb->pub.fd, OOP_WRITE, write_cb, scb);
      scb->write_cb_registered = 1;
    }
  }
  else
    isc_cancel_write_callback(scb);
}
