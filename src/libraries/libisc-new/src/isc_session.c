/*
** isc_session.c                              Routines to handle ISC sessions
**
** Copyright (C) 1991-1993, 1998-1999, 2001 by Peter Eriksson and
** Per Cederqvist of the Lysator Academic Computer Association.
**
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Library General Public
** License as published by the Free Software Foundation; either
** version 2 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
** Library General Public License for more details.
**
** You should have received a copy of the GNU Library General Public
** License along with this library; if not, write to the Free
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
**
** history:
** 910305 pen      moved into separate file
** 920129 pen      added support for "lazy" connect()
** 920209 pen      reworked some of the code
** 920209 pen      TCP and UDP specific code removed.
** 930117 pen      Two memory leak bugs fixed (found by Ceder)
** (See ChangeLog for recent history)
*/

#include <sys/types.h>
#include <sys/socket.h>
#include <errno.h>
#include <time.h>
#ifdef HAVE_STDDEF_H
#  include <stddef.h>
#endif
#ifdef HAVE_STDARG_H
#  include <stdarg.h>
#endif
#include <fcntl.h>
#ifndef NULL
#  include <stdio.h>
#endif
#ifdef HAVE_UNISTD_H
#  include <unistd.h>
#endif
#include <assert.h>

#include "oop.h"
#include "adns.h"
#include "oop-adns.h"

#include "s-string.h"
#include "timeval-util.h"

#include "isc.h"
#include "intern.h"
#include "unused.h"


static oop_call_time stale_cb;

/*
** Insert a session into a master control structure
*/
void
isc_insert(struct isc_mcb *mcb,
	   struct isc_scb_internal *scb)
{
  struct isc_scb_entry *isl;


  ISC_XNEW(isl);
  isl->scb = scb;

  /* Better check if this is the first session or not */
  if (mcb->sessions != NULL)
  {
    isl->next           = mcb->sessions;
    isl->prev           = mcb->sessions->prev;
    isl->prev->next     = isl;
    mcb->sessions->prev = isl;
  }
  else
  {
    isl->next = isl;
    isl->prev = isl;
  }
  
  mcb->sessions  = isl;
}



/*
** Locate a session list entry in a MCB
*/
static struct isc_scb_entry *
isc_findsession(struct isc_mcb *mcb,
		struct isc_scb_internal *scb)
{
  struct isc_scb_entry *isl;


  /* Locate the session in the MCB */
  for (isl = mcb->sessions; isl != NULL && isl->scb != scb; isl = isl->next)
    if (isl->next == mcb->sessions)
      break;

  /* Not found? Return -1 */
  if (isl == NULL || isl->scb != scb)
    return NULL;

  return isl;
}



/*
** Remove a session from a master control structure
*/
int
isc_remove(struct isc_mcb *mcb,
	   struct isc_scb_internal *scb)
{
  struct isc_scb_entry *isl;


  isl = isc_findsession(mcb, scb);
  if (!isl)
  {
    errno = ENOENT;
    return -1;
  }
  
  /* Remove it from the list of sessions if it is still on it */
  if (isl->prev != NULL)
  {
    if (isl->next != isl)
    {
      /* Make sure noone references this session */
      if (mcb->sessions == isl)
	mcb->sessions = isl->next;

      isl->prev->next = isl->next;
      isl->next->prev = isl->prev;
    }
    else
    {
      /* This was the last session on the circular list. */
      mcb->sessions = NULL;
    }
    
    isl->prev = NULL;
    isl->next = NULL;
  }

  isc_free(isl);
  
  return 0;
}


/*
** Create a new session structure. 
*/
struct isc_scb_internal *
isc_create(struct isc_mcb *mcb,
	   struct isc_session_cfg *cfg,
	   enum isc_session_state initial_state)
{
  struct isc_scb_internal  * scb;
  
  
  ISC_XNEW(scb);

  scb->pub.fd       = -1;
  scb->state    = initial_state;
  scb->wr_msg_q = NULL; 
  scb->accept_cb= NULL;
  scb->write_cb_registered = 0;
  scb->stale_output_cb_registered = 0;
  scb->idle_cb_registered = 0;
  scb->data_available_registered = 0;
  scb->data_available_callback = NULL;
  scb->stale_output_cb = NULL;
  scb->acceptable_idle_time = cfg->idle_timeout;
  scb->resolve_callback = NULL;
  scb->adns_query = NULL;
  scb->pub.remote = EMPTY_STRING;

  scb->cfg = cfg;
  scb->pub.master = mcb;
  
  /* Fill in the session structure */
  scb->wr_msg_q      = isc_newqueue();
  scb->sendindex     = 0;

  return scb;
}


static void *
stale_cb(oop_source *UNUSED(src),
	 struct timeval UNUSED(tv),
	 void *user)
{
  struct isc_scb_internal *scb = user;

  scb->stale_output_cb_registered = 0;
  if (scb->stale_output_cb == NULL)
      return OOP_HALT;

  scb->stale_output_cb(&scb->pub);
  return OOP_CONTINUE;
}

static void *
idle_cb(oop_source *UNUSED(src),
	struct timeval UNUSED(tv),
	void *user)
{
  struct isc_scb_internal *scb = user;

  scb->idle_cb_registered = 0;
  if (scb->idle_cb == NULL)
      return OOP_HALT;

  scb->idle_cb(&scb->pub);
  return OOP_CONTINUE;
}

static void
isc_cancel_stale_output_callback(struct isc_scb_internal *session)
{
  oop_source *source = session->pub.master->event_source;
      
  if (session->stale_output_cb_registered)
  {
    source->cancel_time(source, session->stale_output_tv, stale_cb, session);
    session->stale_output_cb_registered = 0;
  }
}

static void
isc_cancel_idle_callback(struct isc_scb_internal *session)
{
  oop_source *source = session->pub.master->event_source;
      
  if (session->idle_cb_registered)
  {
    source->cancel_time(source, session->idle_tv, idle_cb, session);
    session->idle_cb_registered = 0;
  }
}

void
isc_set_read_callback(struct isc_scb *scb,
		      oop_call_fd *data_available_callback,
		      isc_write_error_cb *write_error_cb,
		      isc_stale_output_cb *stale_output_cb,
		      isc_stale_output_cb *idle_output_cb)
{
  struct isc_scb_internal *session = (struct isc_scb_internal*)scb;

  isc_cancel_read_callback(session);
  isc_cancel_stale_output_callback(session);
  isc_cancel_idle_callback(session);

  session->data_available_callback = data_available_callback;
  session->write_err_cb = write_error_cb;
  session->stale_output_cb = stale_output_cb;
  session->idle_cb = idle_output_cb;
  isc_check_read_callback(session, 0);
}

void
isc_set_acceptable_idle(struct isc_scb *scb,
			struct timeval acceptable)
{
  struct isc_scb_internal *session = (struct isc_scb_internal*)scb;

  isc_cancel_idle_callback(session);
  session->acceptable_idle_time = acceptable;
  isc_check_read_callback(session, 0);
}

void
isc_check_read_callback(struct isc_scb_internal *session,
			int any_written)
{
  oop_source *source = session->pub.master->event_source;

  int want_read = 1;
  int want_write_timeout = 0;

  /* Too many queued blocks or bytes?  Don't read any more data, until
     the client has read the output we have already produced. */
  if (session->wr_msg_q != NULL
      && (isc_sizequeue(session->wr_msg_q) >= session->cfg->max.queuedsize
	  || session->wr_msg_q->bytes >= session->cfg->max.queuedsize_bytes))
  {
    want_read = 0;
    want_write_timeout = 1;
  }

  if (session->state == ISC_STATE_DISABLED)
    want_read = 0;

  /* If this session is closing down there is no need for callbacks.  */
  if (session->state == ISC_STATE_CLOSING)
  {
    want_read = 0;
    want_write_timeout = 0;
  }

  if (!want_read)
    isc_cancel_read_callback(session);
  else if (!session->data_available_registered
	   && session->data_available_callback != NULL)
  {
    source->on_fd(source, session->pub.fd, OOP_READ,
		  session->data_available_callback,
		  &session->pub);
    session->data_available_registered = 1;
  }

  if (any_written || !want_write_timeout)
    isc_cancel_stale_output_callback(session);

  if (any_written)
    isc_cancel_idle_callback(session);

  if (want_write_timeout
      && !session->stale_output_cb_registered
      && session->stale_output_cb != NULL)
  {
    setup_timer(&session->stale_output_tv,
		session->pub.master->scfg->stale_timeout);
    source->on_time(source, session->stale_output_tv, stale_cb, session);
    session->stale_output_cb_registered = 1;
  }

  if (!session->idle_cb_registered && session->state != ISC_STATE_CLOSING)
  {
    setup_timer(&session->idle_tv, session->acceptable_idle_time);
    source->on_time(source, session->idle_tv, idle_cb, session);
    session->idle_cb_registered = 1;
  }
}

void
isc_cancel_read_callback(struct isc_scb_internal *session)
{
  oop_source *source = session->pub.master->event_source;

  if (session->data_available_registered)
  {
    source->cancel_fd(source, session->pub.fd, OOP_READ);
    session->data_available_registered = 0;
  }
}

void
isc_cancel_write_callback(struct isc_scb_internal *session)
{
  oop_source *source = session->pub.master->event_source;
      
  if (session->write_cb_registered)
  {
    source->cancel_fd(source, session->pub.fd, OOP_WRITE);
    session->write_cb_registered = 0;
  }
}


/*
** Destroy the allocated Session structure. Close the
** socket if open and if Master structure specified, remove
** the session from it.
*/
int
isc_destroy(struct isc_mcb *mcb,
	    struct isc_scb *scb)
{
  struct isc_scb_internal *sci = (struct isc_scb_internal*)scb;
  isc_resolve_done_cb *cb;
  int code = 0;
  int size;

  if (sci->resolve_callback != NULL)
  {
    if (sci->adns_query != NULL)
      oop_adns_cancel(sci->adns_query);
    else
      mcb->event_source->cancel_time(mcb->event_source, OOP_TIME_NOW,
				     isc_dns_resolve_cb, scb);

    cb = sci->resolve_callback;
    sci->resolve_callback = NULL;
    cb(scb, isc_resolve_aborted, 0);
  }

  isc_cancel_read_callback(sci);
  isc_cancel_stale_output_callback(sci);
  isc_cancel_idle_callback(sci);
  isc_cancel_write_callback(sci);
  s_clear(&scb->remote);

  if (mcb)
  {
    code = isc_remove(mcb, sci);
    if (code < 0)
      return code;
    if (sci->accept_cb != NULL)
    {
      mcb->event_source->cancel_fd(mcb->event_source, scb->fd, OOP_READ);
      sci->accept_cb = NULL;
    }
  }

  assert(sci->accept_cb == NULL);

  if (scb->raddr)
  {
    isc_freeaddress(scb->raddr);
    scb->raddr = NULL;
  }
  
  if (scb->laddr)
  {
    isc_freeaddress(scb->laddr);
    scb->laddr = NULL;
  }
  
  if (scb->fd != -1)
  {
    close(scb->fd);
    scb->fd = -1;
  }

  if (sci->wr_msg_q)
  {
    size = isc_killqueue(sci->wr_msg_q);
    if (mcb != NULL && mcb->write_change_cb)
  	  mcb->write_change_cb(-size);
  }

  isc_free(scb);

  return 0;
}

int isc_disable(struct isc_scb *scb)
{
  struct isc_scb_internal *sci = (struct isc_scb_internal*)scb;

  if (sci->state != ISC_STATE_RUNNING)
    return -1;
  
  sci->state = ISC_STATE_DISABLED;
  isc_check_read_callback(sci, 0);
  return 0;
}



int isc_enable(struct isc_scb *scb)
{
  struct isc_scb_internal *sci = (struct isc_scb_internal*)scb;

  if (sci->state != ISC_STATE_DISABLED)
    return -1;
  
  sci->state = ISC_STATE_RUNNING;
  isc_check_read_callback(sci, 0);
  return 0;
}

oop_source *
isc_getoopsource(struct isc_scb *scb)
{
  return scb->master->event_source;
}
