/*
** isc_stdout.c                 Some nice-to-have functions for output.
**
** Copyright (C) 1991, 1998-1999 by Peter Eriksson and
** Per Cederqvist of the Lysator Academic Computer Association.
**
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Library General Public
** License as published by the Free Software Foundation; either
** version 2 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
** Library General Public License for more details.
**
** You should have received a copy of the GNU Library General Public
** License along with this library; if not, write to the Free
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
**
** history:
** 910305 pen      moved into separate file
** (See ChangeLog for recent history)
*/

#include <stdio.h>
#ifdef HAVE_STRING_H
#  include <string.h>
#endif
#ifdef HAVE_STDARG_H
#  include <stdarg.h>
#endif
#include <time.h>
#include <sys/types.h>
#include <sys/socket.h>

#include "oop.h"
#include "adns.h"
#include "oop-adns.h"

#include "s-string.h"

#include "isc.h"
#include "intern.h"

int
isc_putc(int           chr,
	 struct isc_scb *session)
{
  struct isc_scb_internal *scb = (struct isc_scb_internal*)session;

  if (scb->state != ISC_STATE_DISABLED &&
      scb->state != ISC_STATE_RUNNING)
    return EOF;
  
  while (scb->sendindex == sizeof(scb->sendbuf)
	 || scb->sendindex >= session->master->scfg->max.msgsize)
    isc_flush(session);

  return scb->sendbuf[scb->sendindex++] = chr;
}



int
isc_write(struct isc_scb  *session,
	  const void  * buffer,
	  size_t        length)
{
  const char  * bp;
  int           len;
  int           blen;
  int           clen;
  size_t        maxlen;
  struct isc_scb_internal *scb = (struct isc_scb_internal*)session;


  if (scb->state != ISC_STATE_DISABLED &&
      scb->state != ISC_STATE_RUNNING)
    return EOF;
  
  bp   = (const char *) buffer;
  clen = length;
  
  maxlen = session->master->scfg->max.msgsize;
  if (sizeof(scb->sendbuf) < maxlen)
    maxlen = sizeof(scb->sendbuf);

  while (clen > 0)
  {
    blen = maxlen - scb->sendindex;

    /* Make room in sendbuf */
    while (blen == 0)
    {
      isc_flush(&scb->pub);
      blen = maxlen - scb->sendindex;
    }

    len  = (clen > blen ? blen : clen);
    
    memcpy(scb->sendbuf+scb->sendindex, bp, len);
    scb->sendindex += len;
    clen -= len;
    bp   += len;
  }

  return length;
}

int
isc_puts(const char *str,
	 struct isc_scb *scb)
{
  return isc_write(scb, str, strlen(str));
}

int
isc_putul(unsigned long nr,
	  struct isc_scb *scb)
{
  static char   buf[sizeof(unsigned long) * 3 + 1];
  char         *cp;

  if (nr < 10)
    return isc_putc("0123456789"[nr], scb);
  else
  {
    cp = buf + sizeof(buf);
    while (nr > 0)
    {
      *--cp = (nr % 10) + '0';
      nr /= 10;
    }
    return isc_write(scb, cp, buf + sizeof(buf) - cp);
  }
}
