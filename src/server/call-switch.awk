#
# $Id: call-switch.awk,v 0.24 2003/08/23 16:38:18 ceder Exp $
# Copyright (C) 1991, 1993-1999, 2002-2003  Lysator Academic Computer Association.
#
# This file is part of the LysKOM server.
# 
# LysKOM is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by 
# the Free Software Foundation; either version 1, or (at your option) 
# any later version.
# 
# LysKOM is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details.
# 
# You should have received a copy of the GNU General Public License
# along with LysKOM; see the file COPYING.  If not, write to
# Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
# or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
# MA 02139, USA.
#
# Please report bugs at http://bugzilla.lysator.liu.se/. 
#
# $Id: call-switch.awk,v 0.24 2003/08/23 16:38:18 ceder Exp $
BEGIN {
    printf("/* Don't edit this file - it is generated automatically");
    printf(" from\n   call-switch.awk and fncdef.txt */\n");
    printf("\tswitch(client->function)\n\t{\n"); 
}
$1 == "#ifdef" {
    printf("#ifdef %s\n", $2);
    next;
}
$1 == "#endif" {
    printf("#endif\n");
    next;
}
$1 != "#" && $1 != "" {
    printf("\tcase call_fnc_%s:\n\t    ", $3);
    if ( $2 == "success" )
	printf("status = %s(", $3);
    else
	printf("res->number = %s(", $3);
    
    num=0;
    string=0;
    c_string=0;
    for ( i = 4; i <= NF; i++)
    {
	if ( i != 4 )
	    printf(", ");
	if ( $i == ":" )
	{
	    printf("&res->%s", $(i+1));
	    break;
	}
	if ( $i == "num" )
	    printf("client->num%d", num++);
	else if ( $i == "string" )
	    printf("client->string%d", string++);
	else if ( $i == "c_string" )
	    printf("client->c_string%d", c_string++);
	else if ( $i == "priv_bits" )
	    printf("client->priv_bits");
	else if ( $i == "conf_type" )
	    printf("client->conf_type");
	else if ( $i == "membership_type" )
	    printf("&client->membership_type");
        else if ( $i == "aux_item_list")
            printf("&client->aux_item_list");
        else if ( $i == "misc_info_list")
            printf("&client->misc_info_list");
	else if ( $i == "time_date" )
	    printf("&client->time");
        else if ( $i == "info" )
            printf("&client->info");
        else if ( $i == "num_list" )
            printf("&client->num_list" );
        else if ( $i == "pers_flags" )
            printf("client->pers_flags");
        else if ( $i == "read_range_list" )
            printf("&client->read_range_list");
	else
	    printf("\n#error in file server/fncdef.txt\n");
    }
		  
    printf(");\n");
    if ( $2 == "number" )
    {
	if ( $3 == "get_time" )
	    printf("\t    status = OK;\n");
	else
	    printf("\t    status = (res->number != 0) ? OK : FAILURE;\n");
    }

    printf("\t    break;\n\n");
}
END {
    printf("\tcase illegal_fnc:\n");
    print("\t    restart_kom(\"unreachable code reached -- illegal_fnc\");");
    printf("\t    break;\n");
    printf("\t}\n");
}
