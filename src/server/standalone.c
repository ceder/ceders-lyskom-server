/*
 * $Id: standalone.c,v 1.10 2003/08/23 16:38:13 ceder Exp $
 * Copyright (C) 1999, 2002-2003  Lysator Academic Computer Association.
 *
 * This file is part of the LysKOM server.
 * 
 * LysKOM is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by 
 * the Free Software Foundation; either version 1, or (at your option) 
 * any later version.
 * 
 * LysKOM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with LysKOM; see the file COPYING.  If not, write to
 * Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
 * or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
 * MA 02139, USA.
 *
 * Please report bugs at http://bugzilla.lysator.liu.se/. 
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <stdlib.h>
#include <setjmp.h>
#include "timewrap.h"

#include "kom-types.h"
#include "async.h"
#include "com.h"
#include "connections.h"
#include "kom-errno.h"
#include "manipulate.h"
#include "unused.h"

/* Dummy routines for standalone programs. */
void 
register_jubel(Pers_no UNUSED(pno),
	       Text_no UNUSED(divis),
	       Text_no UNUSED(tno),
	       Bool    UNUSED(public))
{
}

Info		  kom_info =
{
#include "version.incl"
    ,				/* version */
    1,				/* conf_pres_conf */
    2,				/* pers_pres_conf */
    3,				/* motd_conf */
    4,				/* kom_news_conf */
    0,				/* motd_of_lyskom */
    0,                          /* highest_aux_no */
    { 0, NULL }                 /* aux_item_list */
};
