# Test suite for lyskomd.
# Copyright (C) 1998-2003  Lysator Academic Computer Association.
#
# This file is part of the LysKOM server.
# 
# LysKOM is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by 
# the Free Software Foundation; either version 1, or (at your option) 
# any later version.
# 
# LysKOM is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details.
# 
# You should have received a copy of the GNU General Public License
# along with LysKOM; see the file COPYING.  If not, write to
# Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
# or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
# MA 02139, USA.
#
# Please report bugs at http://bugzilla.lysator.liu.se/. 


# Check that all functions do the right thing when invoked before the
# user logs in.

read_versions

lyskomd_start

client_start 0
talk_to client 0
send "A\n"
send "[holl "DejaGnu test suite"]\n"
simple_expect "LysKOM" "connected"

send "1000 62 5 [holl "bogus"] 0\n"
simple_expect "%1000 4 5"

send "1001 62 4 [holl "nono"] 0\n"
simple_expect "%1001 10 4"

send "1002 62 6 [holl "nono"] 0\n"
simple_expect "%1002 10 6"

send "1003 62 60 [holl "nono"] 0\n"
simple_expect "%1003 10 60"

send "1004 62 0 [holl "zero"] 0\n"
simple_expect "%1004 8 0"

# Turn of async messages
send "1005 80 0 { }\n"
simple_expect "=1005"

# Test that most calls fail when the user isn't logged in.

# 0:login-old may succeed
# 1:logout always succeeds
send "1006 1\n"
simple_expect "=1006" "logout"
# 2:change-conference
send "1007 2 4\n"
simple_expect "%1007 6 0" "change-conference"
# 3:change-name
send "1008 3 1 [holl "new name"]\n"
simple_expect "%1008 6 0" "change-name"
# 4:change-what-i-am-doing
send "1009 4 [holl "testing lyskomd"]\n"
simple_expect "=1009" "change-what-i-am-doing"
# 5:create-person-old
send "1010 5 [holl "new person"] [holl "mypasswd"]\n"
simple_expect "=1010 6" "create-person-old"
send "1011 39\n"
simple_expect "=1011 1 { 6 0 [holl ""] }" "create-person-old"
send "1012 1\n"
simple_expect "=1012" "create-person-old"
# 6:get-person-stat-old
send "1013 6 6 1\n"
simple_expect "%1013 6 0" "get-person-stat-old"
# 7:set-priv-bits
send "1014 7 6 0000000000000000\n"
simple_expect "%1014 6 0" "set-priv-bits"
# 8:set-passwd
send "1015 8 5 [holl "old"] [holl "new"]\n"
simple_expect "%1015 6 0" "set-passwd"
# 9:query-read-texts-old
send "1016 9 6 6\n"
simple_expect "=1016 $any_time 6 255 0 0 \\*" "query-read-texts-old"
# 10:create-conf-old
send "1017 10 [holl "no way"] 0000\n" 
simple_expect "%1017 6 0" "create-conf-old"
# 11:delete-conf
send "1018 11 1\n"
simple_expect "%1018 6 0" "delete-conf"
# 12:lookup-name
send "1019 12 [holl "pres"]\n"
simple_expect "=1019 2 { 1 2 } { 0000 0000 }" "lookup-name"
send "1020 12 [holl "xyzzy does not exist"]\n"
simple_expect "=1020 0 \\* \\*" "lookup-name"
# 13:get-conf-stat-older
send "1021 13 1 0\n"
simple_expect "=1021 0H 0000 $any_time $any_time 0 0 0 0 0 0 77 0 1 0" "get-conf-stat-older"
send "1022 13 1 1\n"
simple_expect "=1022 [holl "Presentation .av nya. m�ten"] 0000 $any_time $any_time 0 0 0 0 0 0 77 0 1 0" "get-conf-stat-older"
# 14:add-member-old
send "1023 14 4 5 255 1\n"
simple_expect "%1023 6 0" "add-member-old"
# 15:sub-member
send "1024 15 4 5\n"
simple_expect "%1024 6 0" "sub-member"
# 16:set-presentation
send "1025 16 4 1\n"
simple_expect "%1025 6 0" "set-presentation"
# 17:set-etc-motd
send "1026 17 4 1\n"
simple_expect "%1026 6 0" "set-etc-motd"
# 18:set-supervisor
send "1027 18 4 1\n"
simple_expect "%1027 6 0" "set-supervisor"
# 19:set-permitted-submitters
send "1028 19 4 1\n"
simple_expect "%1028 6 0" "set-permitted-submitters"
# 20:set-super-conf
send "1029 20 4 1\n"
simple_expect "%1029 6 0" "set-super-conf"
# 21:set-conf-type
send "1030 21 4 0000\n"
simple_expect "%1030 6 0" "set-conf-type"
send "1031 21 4 00000000\n"
simple_expect "%1031 6 0" "set-conf-type"
# 22:set-garb-nice
send "1032 22 4 17\n"
simple_expect "%1032 6 0" "set-garb-nice"
# 23:get-marks
send "1033 23\n"
simple_expect "%1033 6 0" "get-marks"
# 24:mark-text-old
send "1034 24 1 243\n"
simple_expect "%1034 6 0" "mark-text-old"
# 25:get-text
send "1035 25 1 0 999\n"
simple_expect "%1035 14 1" "get-text"
# 26:get-text-stat-old
send "1036 26 1\n"
simple_expect "%1036 14 1" "get-text-stat-old"
# 27:mark-as-read
send "1037 27 3 1 { 1 }\n"
simple_expect "%1037 6 0" "mark-as-read"
# 28:create-text-old
send "1038 28 [holl "A sample text"] 1 { 0 2 }\n"
simple_expect "%1038 6 0" "create-text-old"
# 29:delete-text
send "1039 29 1\n"
simple_expect "%1039 6 0" "delete-text"
# 30:add-recipient
send "1040 30 1 4 0\n"
simple_expect "%1040 6 0" "add-recipient"
# 31:sub-recipient
send "1041 31 1 4\n"
simple_expect "%1041 6 0" "sub-recipient"
# 32:add-comment
send "1042 32 2 1\n"
simple_expect "%1042 6 0" "add-comment"
# 33:sub-comment
send "1043 33 2 1\n"
simple_expect "%1043 6 0" "sub-comment"
# 34:get-map
send "1044 34 1 0 99\n"
simple_expect "%1044 6 0" "get-map"
# 35:get-time
send "1045 35\n"
simple_expect "=1045 $any_time" "get-time"
# 36:get-info-old
send "1046 36\n"
simple_expect "=1046 $server_compat_version 1 2 3 4 0" "get-info-old"
# 37:add-footnote
send "1047 37 1 2\n"
simple_expect "%1047 6 0" "add-footnote"
# 38:sub-footnote
send "1048 38 1 2\n"
simple_expect "%1048 6 0" "sub-footnote"
# 39:who-is-on-old
send "1049 39\n"
simple_expect "=1049 0 \\*" "who-is-on-old"
# 40:set-unread
send "1050 40 1 3\n"
simple_expect "%1050 6 0" "set-unread"
# 41:set-motd-of-lyskom
send "1051 41 1\n"
simple_expect "%1051 6 0" "set-motd-of-lyskom"
# 42:enable
send "1052 42 255\n"
simple_expect "%1052 6 0" "enable"
# 43:sync-kom
send "1053 43\n"
simple_expect "%1053 6 0" "sync-kom"
# 44:shutdown-kom
send "1054 44 2\n"
simple_expect "%1054 6 0" "shutdown-kom"
# 45:broadcast
send "1055 45 [holl "broadcast-message"]\n"
simple_expect "%1055 6 0" "broadcast"
# 46:get-membership-old
send "1056 46 5 1 3 1\n"
simple_expect "%1056 6 0" "get-membership-old"
# 47:get-created-texts
send "1057 47 5 1 3\n"
simple_expect "%1057 6 0" "get-created-text"
# 48:get-members-old
send "1058 48 5 0 1\n"
simple_expect "=1058 1 { 5 }" "get-members-old"
send "1059 48 5 0 2\n"
simple_expect "=1059 1 { 5 }" "get-members-old"
send "1060 48 5 0 10\n"
simple_expect "=1060 1 { 5 }" "get-members-old"
send "1061 49 5\n"
simple_expect "=1061 [holl "X.unknown.@localhost"] 1111111111111111 00000000 $any_time 0 482 4 0 0 0 0 0 0 1 0 0 1" "get-members-old"
# 50:get-conf-stat-old
send "1062 50 1\n"
simple_expect "=1062 [holl "Presentation .av nya. m�ten"] 0000 $any_time $any_time 0 0 0 0 0 0 77 0 1 0" "get-conf-stat-old"
# 51:who-is-on
send "1063 51\n"
simple_expect "=1063 0 \\*" "who-is-on"
# 52:get-unread-confs
send "1064 52 5\n"
simple_expect "%1064 6 0" "get-unread-confs"
# 53:send-message
send "1065 53 1 [holl "holler"]\n"
simple_expect "%1065 6 0" "send-message"
# 54:get-session-info
send "1066 54 1\n"
simple_expect "%1066 6 0" "get-session-info"
# 55:disconnect
send "1067 55 2\n"
simple_expect "%1067 6 0" "disconnect"
# 56:who-am-i
send "1068 56\n"
simple_expect "=1068 1" "who-am-i"
# 57:set-user-area
send "1069 57 5 2\n"
simple_expect "%1069 6 0" "set-user-area"
# 58:get-last-text
send "1070 58 0 0 12 1 0 90 0 0 0\n"
simple_expect "%1070 6 0" "get-last-text"
# 59:create-anonymous-text-old
send "1071 59 [holl "anon-txt"] 1 { 0 2 }\n"
simple_expect "%1071 6 0" "create-anonymous-text-old"
# 60:find-next-text-no
send "1072 60 0\n"
simple_expect "%1072 6 0" "find-next-text-no"
send "1073 60 1\n"
simple_expect "%1073 6 0" "find-next-text-no"
send "1074 60 3\n"
simple_expect "%1074 6 0" "find-next-text-no"
# 61:find-previous-text-no
send "1075 61 18\n"
simple_expect "%1075 6 0" "find-previous-text-no"
# 62:login
send "1076 62 0 [holl "broken"] 1\n"
simple_expect "%1076 8 0" "login"
# 63:who-is-on-ident
send "1077 63\n"
simple_expect "=1077 0 \\*" "who-is-on-ident"
# 64:get-session-info-ident
send "1078 64 1\n"
simple_expect "%1078 6 0" "get-session-info-ident"
# 65:re-lookup-person
send "1079 65 [holl "."]\n"
simple_expect "=1079 2 { 5 6 }" "re-lookup-person"
# 66:re-lookup-conf
send "1080 66 [holl "Pre.*m.*"]\n"
simple_expect "=1080 2 { 1 2 }" "re-lookup-conf"
# 67:lookup-person
send "1081 67 [holl "ad"]\n"
simple_expect "=1081 1 { 5 }" "lookup-person"
# 68:lookup-conf
send "1082 68 [holl "n o lys"]\n"
simple_expect "=1082 1 { 4 }" "lookup-conf"
# 69:set-client-version
send "1083 69 [holl "DejaGnu"] [holl "1.3"]\n"
simple_expect "=1083" "set-client-version"
# 70:get-client-name
send "1084 70 1\n"
simple_expect "%1084 6 0" "get-client-name"
# 71:get-client-version
send "1085 71 1\n"
simple_expect "%1085 6 0" "get-clien-version"
# 72:mark-text
send "1086 72 1 239\n"
simple_expect "%1086 6 0" "mark-text"
# 73:unmark-text
send "1087 73 1\n"
simple_expect "%1087 6 0" "unmark-text"
# 74:re-z-lookup
send "1088 74 [holl "LysKOM"] 1 1\n"
simple_expect "=1088 2 { [holl "Nyheter om LysKOM"] 0000 4 [holl "Administrat�r .f�r. LysKOM"] 1001 5 }" "re-z-lookup"
# 75:get-version-info
send "1089 75\n"
simple_expect "=1089 $protocol_a_level [holl "$server_software"] [holl "$server_version"]" "get-version-info"
# 76:lookup-z-name
send "1090 76 [holl "pre m"] 1 1\n"
simple_expect "=1090 2 { [holl "Presentation .av nya. m�ten"] 0000 1 [holl "Presentation .av nya. medlemmar"] 0000 2 }" "lookup-z-name"
# 77:set-last-read
send "1091 77 3 4\n"
simple_expect "%1091 6 0" "set-last-read"
# 78:get-uconf-stat
send "1092 78 3\n"
simple_expect "=1092 [holl "Lappar .p�. d�rren"] 00001000 0 77" "get-uconf-stat"
# 79:set-info
send "1093 79 10901 1 2 3 4 1080\n"
simple_expect "%1093 6 0" "set-info"
# 80:accept-async
send "1094 80 0 { }\n"
simple_expect "=1094" "accept-async"
# 81:query-async
send "1095 81\n"
simple_expect "=1095 0 \\*" "query-async"
# 82:user-active
send "1096 82\n"
simple_expect "=1096" "user-active"
# 83:who-is-on-dynamic
send "1097 83 1 1 0\n"
simple_expect "=1097 1 { 1 0 0 $any_num 11000000 [holl ""] }" "who-is-on-dynamic"
# 84:get-static-session-info
send "1098 84 1\n"
simple_expect "%1098 6 0" "get-static-session-info"
# 85:get-collate-table
send "1099 85\n"
unanchored_expect "^MRK:client0: =1099 256H" "get-collate-table start"
unanchored_expect "$nl" "get-collate-table newline before digits"
# NOTE: Comment out the next two lines if runtest seems to hang.
# NOTE: But also notify ceder of the versions of dejagnu, TCL and expect
# NOTE: that you are using, and try to upgrade to current versions first.
unanchored_expect "0123456789" "get-collate-table digits"
unanchored_expect "ABCDEFGHIJKLMNOPQRSTUVWXYZ" "get-collate-table upper"
unanchored_expect "ABCDEFGHIJKLMNOPQRSTUVWXYZ" "get-collate-table lower"
unanchored_expect "\377$nl" "collate end"
# 86:create-text
send "1100 86 [holl "some text"] 1 { 0 4 } 0 { }\n"
simple_expect "%1100 6 0" "create-text"
# 87:create-anonymous-text
send "1101 87 [holl "some text"] 1 { 0 4 } 0 { }\n"
simple_expect "%1101 6 0" "create-anonymous-text"
# 88:create-conf
send "1102 88 [holl "new conf"] 0000 0 { }\n" 
simple_expect "%1102 6 0" "create-conf"
send "1103 88 [holl "new conf"] 00000000 0 { }\n"
simple_expect "%1103 6 0" "create-conf"
# 89:create-person
send "1104 89 [holl "new 89 person"] [holl "89pwd"] 00000000 0 { }\n"
simple_expect "=1104 7" "create-person"
  send "101105 62 7 [holl "89pwd"] 0\n"
  simple_expect "=101105"
send "1105 83 1 1 0\n"
simple_expect "=1105 1 { 1 7 0 $any_num 01000000 [holl ""] }" "create-person"
send "1106 1\n"
simple_expect "=1106" "create-person"
send "1107 83 1 1 0\n"
simple_expect "=1107 1 { 1 0 0 $any_num 11000000 [holl ""] }" "create-person"
# 90:get-text-stat
send "1108 90 1\n"
simple_expect "%1108 14 1" "get-text-stat"
# 91:get-conf-stat
send "1109 91 1\n"
simple_expect "=1109 [holl "Presentation .av nya. m�ten"] 00001000 $any_time $any_time 0 0 0 0 0 0 77 77 0 1 0 0 0 \\*" "get-conf-stat"
# 92:modify-text-info
send "1110 92 1 0 { } 0 { }\n"
simple_expect "%1110 6 0" "modify-text-info"
# 93:modify-conf-info
send "1111 93 1 0 { } 0 { }\n"
simple_expect "%1111 6 0" "modify-conf-info"
# 94:get-info
send "1112 94\n"
simple_expect "=1112 $server_compat_version 1 2 3 4 0 0 \\*" "get-info"
# 95:modify-system-info
send "1113 95 0 { } 0 { }\n"
simple_expect "%1113 6 0" "modify-system-info"
# 96:query-predefined-aux-items
send "1114 96\n"
simple_expect "=1114 40 { 10104 10103 10102 10101 10100 35 34 33 32 31 30 29 28 27 26 25 24 23 22 21 20 19 18 17 16 15 14 13 12 11 10 9 8 7 6 5 4 3 2 1 }" "query-predefined-aux-items"
# 97:set-expire
send "1115 97 1 76\n"
simple_expect "%1115 6 0" "set-expire"
# 98:query-read-texts-10
send "1116 98 6 6\n"
simple_expect "=1116 0 $any_time 6 255 0 0 \\* 6 $any_time 00000000" "query-read-texts-10"
# 99:get-membership-10
send "1117 99 6 0 10 1\n"
simple_expect "%1117 6 0" "get-membership-10"
# 100:add-member
send "1118 100 5 3 250 1 00000000\n"
simple_expect "%1118 6 0" "add-member"
# 101:get-members
send "1119 101 5 0 10\n"
simple_expect "=1119 1 { 5 5 $any_time 00000000 }" "get-members"
# 102:set-membership-type
send "1120 102 5 5 01000000\n"
simple_expect "%1120 6 0" "set-membership-type"
# 103:local-to-global
send "1121 103 1 1 20\n"
simple_expect "%1121 6 0" "local-to-global"
# 104:map_created_texts
send "1122 104 5 1 20\n"
simple_expect "%1122 6 0" "map-created-texts"
# 105:set-keep-commented
send "1123 105 1 99\n"
simple_expect "%1123 6 0" "set-keep-commented"
# 106:set-pers-flags
send "1124 106 5 10101010\n"
simple_expect "%1124 6 0" "set-pers-flags"
# 107:query-read-texts
send "1125 107 6 6 1 0\n"
simple_expect "=1125 0 $any_time 6 255 0 \\* 6 $any_time 00000000" "query-read-texts"
# 108:get-membership
send "1126 108 6 0 10 1 0\n"
simple_expect "%1126 6 0" "get-membership"
# 109:mark-as-unread
send "1127 109 6 1\n"
simple_expect "%1127 6 0" "mark-as-unread"
# 110:set-read-ranges
send "1128 110 6 0 { }\n"
simple_expect "%1128 6 0" "mark-as-unread"
send "1129 110 6 1 { 1 2 }\n"
simple_expect "%1129 6 0" "mark-as-unread"
send "1130 110 6 2 { 1 1 3 3 }\n"
simple_expect "%1130 6 0" "mark-as-unread"
send "1131 110 6 1 { 3 1 }\n"
simple_expect "%1131 55 0" "mark-as-unread"
send "1132 110 6 2 { 1 3 3 3 }\n"
simple_expect "%1132 56 1" "mark-as-unread"
send "1133 110 6 2 { 0 1 3 3 }\n"
simple_expect "%1133 17 0" "mark-as-unread"
send "1134 110 6 2 { 1 1 3 2 }\n"
simple_expect "%1134 55 1" "mark-as-unread"

# 111:get-stats-description
send "1135 111\n"
simple_expect "=1135 10 { [holl "run-queue-length"] [holl "pending-dns"] [holl "pending-ident"] [holl "clients"] [holl "reqs"] [holl "texts"] [holl "confs"] [holl "persons"] [holl "send-queue-bytes"] [holl "recv-queue-bytes"] } 6 { 0 1 15 60 300 900 }"
# 112:get-stats
send "1136 112 [holl "run-queue-length"]\n"
simple_expect "=1136 6 { 0 0 0 $any_float $any_float $any_float $any_float $any_float $any_float $any_float $any_float $any_float $any_float $any_float $any_float $any_float $any_float $any_float }"
send "1137 112 [holl "pending-dns"]\n"
simple_expect "=1137 6 { 0 0 0 $any_float $any_float $any_float $any_float $any_float $any_float $any_float $any_float $any_float $any_float $any_float $any_float $any_float $any_float $any_float }"
send "1138 112 [holl "pending-ident"]\n"
simple_expect "=1138 6 { 0 0 0 $any_float $any_float $any_float $any_float $any_float $any_float $any_float $any_float $any_float $any_float $any_float $any_float $any_float $any_float $any_float }"
send "1139 112 [holl "clients"]\n"
simple_expect "=1139 6 { 1 0 0 $any_float $any_float $any_float $any_float $any_float $any_float $any_float $any_float $any_float $any_float $any_float $any_float $any_float $any_float $any_float }"
send "1140 112 [holl "reqs"]\n"
simple_expect "=1140 6 { 1 0 0 $any_float $any_float $any_float $any_float $any_float $any_float $any_float $any_float $any_float $any_float $any_float $any_float $any_float $any_float $any_float }"
send "1141 112 [holl "texts"]\n"
simple_expect "=1141 6 { 0 0 0 $any_float $any_float $any_float $any_float $any_float $any_float $any_float $any_float $any_float $any_float $any_float $any_float $any_float $any_float $any_float }"
send "1142 112 [holl "confs"]\n"
simple_expect "=1142 6 { 7 0 0 $any_float $any_float $any_float $any_float $any_float $any_float $any_float $any_float $any_float $any_float $any_float $any_float $any_float $any_float $any_float }"
send "1143 112 [holl "persons"]\n"
simple_expect "=1143 6 { 3 0 0 $any_float $any_float $any_float $any_float $any_float $any_float $any_float $any_float $any_float $any_float $any_float $any_float $any_float $any_float $any_float }"
send "1144 112 [holl "send-queue-bytes"]\n"
simple_expect "=1144 6 { $any_num 0 0 $any_float $any_float $any_float $any_float $any_float $any_float $any_float $any_float $any_float $any_float $any_float $any_float $any_float $any_float $any_float }"
send "1145 112 [holl "recv-queue-bytes"]\n"
simple_expect "=1145 6 { $any_num 0 0 $any_float $any_float $any_float $any_float $any_float $any_float $any_float $any_float $any_float $any_float $any_float $any_float $any_float $any_float $any_float }"
# Non-existing stuff.
send "1146 112 [holl "no-such-thing"]\n"
simple_expect "%1146 57 0"

# 113:get-boottime-info
send "1147 113\n"
simple_expect "=1147 $any_time $any_time [holl "clean"] 0 0 5 1 5"

# 114:first-unused-conf-no
send "1148 114\n"
simple_expect "=1148 8"

# 115:first-unused-text-no
send "1149 115\n"
simple_expect "=1149 1"

# 116:find-next-conf-no
send "1150 116 0\n"
simple_expect "%1150 6 0"
send "1151 116 1\n"
simple_expect "%1151 6 0"
send "1152 116 2\n"
simple_expect "%1152 6 0"
send "1153 116 200\n"
simple_expect "%1153 6 0"

# 117:find-previous-conf-no
send "1154 117 0\n"
simple_expect "%1154 6 0"
send "1155 117 1\n"
simple_expect "%1155 6 0"
send "1156 117 2\n"
simple_expect "%1156 6 0"
send "1157 117 200\n"
simple_expect "%1157 6 0"

# 118:get-scheduling
send "1158 118 0\n"
simple_expect "=1158 0 20"
send "1159 118 1\n"
simple_expect "=1159 0 20"
send "1160 118 2\n"
simple_expect "%1160 6 0"

# 119:set-scheduling
send "1161 119 0 0 1\n"
simple_expect "=1161"
send "1162 119 2 0 1\n"
simple_expect "%1162 6 0"
send "1163 119 1 0 1\n"
simple_expect "=1163"
send "1164 119 1 1 1\n"
simple_expect "%1164 19 0"
send "1165 119 1 0 0\n"
simple_expect "%1165 60 0"
send "1166 119 1 0 101\n"
simple_expect "%1166 59 100"

# 120:set-connection-time-format
send "1167 120 1\n"
simple_expect "=1167"
send "1168 120 2\n"
simple_expect "%1168 61 0"
send "1169 120 0\n"
simple_expect "=1169"

# 121:local-to-global-reverse
send "1170 121 1 1 20\n"
simple_expect "%1170 6 0"
# 122:map-created-texts-reverse
send "1171 122 5 1 20\n"
simple_expect "%1171 6 0"

# 123:does not exist (change this when you add a call)
send "1172 123\n"
simple_expect "%1172 2 0"

# finally, check that 55=disconnect actually works without logging in.
send "1173 55 1\n"
simple_expect "=1173" "disconnect (no login)"
client_death 0

client_start 0
talk_to client 0
send "A\n"
send "[holl "DejaGnu test suite"]\n"
simple_expect "LysKOM" "connected"
# Turn of async messages
send "1174 80 0 { }\n"
simple_expect "=1174"

# Log in
send "1175 62 5 [holl "gazonk"] 0\n"
simple_expect "=1175"

send "1176 9 5 5\n"
simple_expect "=1176 $any_time 5 255 0 0 \\*"

send "1177 27 5 0 { }\n"
simple_expect "=1177"

send "1178 9 5 5\n"
simple_expect "=1178 $any_time 5 255 0 0 \\*"

send "1179 6 6 1\n"
simple_expect "=1179 [idholl "DejaGnu test suite.unknown."] 0000010000000000 00000000 $any_time 0 $any_num 1 0 0 0 0 0 0 1 0 0 1"
send "1180 6 6 0\n"
simple_expect "=1180 0H 0000010000000000 00000000 $any_time 0 $any_num 1 0 0 0 0 0 0 1 0 0 1"

talk_to client 0

send "1181 42 255\n"
simple_expect "=1181" "42=enable succeeded"
send "1182 44 0\n"
simple_expect "=1182" "44=shutdown-kom succeeded"
client_death 0
lyskomd_death
