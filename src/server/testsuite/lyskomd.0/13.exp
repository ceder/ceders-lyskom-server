# Test suite for lyskomd.
# Copyright (C) 1999, 2001, 2003  Lysator Academic Computer Association.
#
# This file is part of the LysKOM server.
# 
# LysKOM is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by 
# the Free Software Foundation; either version 1, or (at your option) 
# any later version.
# 
# LysKOM is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details.
# 
# You should have received a copy of the GNU General Public License
# along with LysKOM; see the file COPYING.  If not, write to
# Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
# or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
# MA 02139, USA.
#
# Please report bugs at http://bugzilla.lysator.liu.se/. 

# Check conference creation, name changes, and name lookup.

read_versions
source "$srcdir/config/prot-a.exp"

lyskomd_start

client_start 0
kom_connect "DejaGnu Test Suite"
kom_accept_async "0 { }"

kom_login 5 "gazonk" 0

kom_create_conference "Match 6" "00000000" "0 { }"
kom_create_conference "Match 7" "00000000" "0 { }"
kom_create_conference "Match 8" "00000000" "0 { }"

send "1000 76 [holl "Match"] 1 1\n"
simple_expect "=1000 3 { [holl "Match 6"] 0000 6 [holl "Match 7"] 0000 7 [holl "Match 8"] 0000 8 }"

kom_create_conference "Match 9" "00000000" "0 { }"
kom_create_conference "Junk 10" "00000000" "0 { }"

send "1001 76 [holl "Match"] 1 1\n"
simple_expect "=1001 4 { [holl "Match 6"] 0000 6 [holl "Match 7"] 0000 7 [holl "Match 8"] 0000 8 [holl "Match 9"] 0000 9 }"

send "1002 3 6 [holl "Junk 6"]\n"
simple_expect "=1002"
lyskomd_expect "User 5 changed the name of conference 6\\."
lyskomd_expect "Old name: 'Match 6'\\."
lyskomd_expect "New name: 'Junk 6'\\."

send "1003 76 [holl "Match"] 1 1\n"
simple_expect "=1003 3 { [holl "Match 7"] 0000 7 [holl "Match 8"] 0000 8 [holl "Match 9"] 0000 9 }"

send "1004 3 8 [holl "Junk 8"]\n"
simple_expect "=1004"
lyskomd_expect "User 5 changed the name of conference 8\\."
lyskomd_expect "Old name: 'Match 8'\\."
lyskomd_expect "New name: 'Junk 8'\\."

send "1005 76 [holl "Match"] 1 1\n"
simple_expect "=1005 2 { [holl "Match 7"] 0000 7 [holl "Match 9"] 0000 9 }"

send "1006 3 7 [holl "Junk 7"]\n"
simple_expect "=1006"
lyskomd_expect "User 5 changed the name of conference 7\\."
lyskomd_expect "Old name: 'Match 7'\\."
lyskomd_expect "New name: 'Junk 7'\\."

send "1007 76 [holl "Match"] 1 1\n"
simple_expect "=1007 1 { [holl "Match 9"] 0000 9 }"

send "1008 76 [holl "Junk"] 1 1\n"
simple_expect "=1008 4 { [holl "Junk 6"] 0000 6 [holl "Junk 7"] 0000 7 [holl "Junk 8"] 0000 8 [holl "Junk 10"] 0000 10 }"

send "1009 11 6\n"
simple_expect "=1009"
lyskomd_expect "User 5 deleted conference 6\\."
lyskomd_expect "Conference 6 was named 'Junk 6'\\."

send "1010 76 [holl "Match"] 1 1\n"
simple_expect "=1010 1 { [holl "Match 9"] 0000 9 }"

send "1011 11 9\n"
simple_expect "=1011"
lyskomd_expect "User 5 deleted conference 9\\."
lyskomd_expect "Conference 9 was named 'Match 9'\\."

send "1012 76 [holl "Junk"] 1 1\n"
simple_expect "=1012 3 { [holl "Junk 7"] 0000 7 [holl "Junk 8"] 0000 8 [holl "Junk 10"] 0000 10 }"

send "1013 76 [holl "Match"] 1 1\n"
simple_expect "=1013 0 \\*"

kom_create_conference "Junk 11" 00000000 "0 { }\n" 

send "1014 76 [holl "Junk"] 1 1\n"
simple_expect "=1014 4 { [holl "Junk 7"] 0000 7 [holl "Junk 8"] 0000 8 [holl "Junk 10"] 0000 10 [holl "Junk 11"] 0000 11 }"

kom_create_conference "Match 12" 00000000 "0 { }\n"

send "1015 76 [holl "Junk"] 1 1\n"
simple_expect "=1015 4 { [holl "Junk 7"] 0000 7 [holl "Junk 8"] 0000 8 [holl "Junk 10"] 0000 10 [holl "Junk 11"] 0000 11 }"

send "1016 76 [holl "Match"] 1 1\n"
simple_expect "=1016 1 { [holl "Match 12"] 0000 12 }"

kom_enable 255
send "9999 44 0\n"
simple_expect "=9999"
client_death 0
lyskomd_death

