# Test suite for lyskomd.
# Copyright (C) 2003  Lysator Academic Computer Association.
#
# This file is part of the LysKOM server.
# 
# LysKOM is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by 
# the Free Software Foundation; either version 1, or (at your option) 
# any later version.
# 
# LysKOM is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details.
# 
# You should have received a copy of the GNU General Public License
# along with LysKOM; see the file COPYING.  If not, write to
# Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
# or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
# MA 02139, USA.
#
# Please report bugs at http://bugzilla.lysator.liu.se/. 

# Test that clients are disconnected properly if they are idle too long.

send_user "All timeouts short\n"

lyskomd_start "" "Stale timeout: 1 seconds
Connect timeout: 4 seconds
Login timeout: 4 seconds
Active timeout: 4 seconds
Sync interval: 1 days"

client_start 0

client_start 1
send "A\n"
send "[holl "DejaGnu test suite"]\n"
simple_expect "LysKOM" "connected"
send "1000 80 0 { }\n"
simple_expect "=1000"

client_start 2
send "A\n"
send "[holl "DejaGnu test suite"]\n"
simple_expect "LysKOM" "connected"
send "1001 80 0 { }\n"
simple_expect "=1001"
send "1002 0 5 [holl "gazonk"]\n"
simple_expect "=1002"

lyskomd_expect "Client 1 from $any* has been idle too long\.  Killing it\."
client_death 0
lyskomd_expect "Client 2 from $any* has been idle too long\.  Killing it\."
client_death 1
lyskomd_expect "Client 3 from $any* has been idle too long\.  Killing it\."
client_death 2

system "kill -TERM $lyskomd_pid"
lyskomd_death "" signal

# =========================================================================

send_user "Connect timeout short\n"

lyskomd_start "" "Stale timeout: 1 seconds
Connect timeout: 4 seconds
Login timeout: 1 hour
Active timeout: 1 hour
Sync interval: 1 days"

client_start 0

client_start 1
send "A\n"
send "[holl "DejaGnu test suite"]\n"
simple_expect "LysKOM" "connected"
send "1003 80 0 { }\n"
simple_expect "=1003"

client_start 2
send "A\n"
send "[holl "DejaGnu test suite"]\n"
simple_expect "LysKOM" "connected"
send "1004 80 0 { }\n"
simple_expect "=1004"
send "1005 0 5 [holl "gazonk"]\n"
simple_expect "=1005"

lyskomd_expect "Client 1 from $any* has been idle too long\.  Killing it\."
client_death 0
sleep 10
talk_to client 1
send "1006 35 \n"
simple_expect "=1006 $any_time"
talk_to client 2
send "1007 35 \n"
simple_expect "=1007 $any_time"

system "kill -TERM $lyskomd_pid"
lyskomd_death "" signal
client_death 1
client_death 2

# =========================================================================

send_user "Login timeout short\n"

lyskomd_start "" "Stale timeout: 1 seconds
Connect timeout: 1 hour
Login timeout: 4 seconds
Active timeout: 1 hour
Sync interval: 1 days"

client_start 0

client_start 1
send "A\n"
send "[holl "DejaGnu test suite"]\n"
simple_expect "LysKOM" "connected"
send "1008 80 0 { }\n"
simple_expect "=1008"

client_start 2
send "A\n"
send "[holl "DejaGnu test suite"]\n"
simple_expect "LysKOM" "connected"
send "1009 80 0 { }\n"
simple_expect "=1009"
send "1010 0 5 [holl "gazonk"]\n"
simple_expect "=1010"

lyskomd_expect "Client 2 from $any* has been idle too long\.  Killing it\."
client_death 1
sleep 10
talk_to client 0
send "A3Hfoo\n"
simple_expect "LysKOM"
talk_to client 2
send "1011 35 \n"
simple_expect "=1011 $any_time"

system "kill -TERM $lyskomd_pid"
lyskomd_death "" signal
client_death 0
client_death 2

# =========================================================================

send_user "Active timeout short\n"

lyskomd_start "" "Stale timeout: 1 seconds
Connect timeout: 1 hour
Login timeout: 1 hour
Active timeout: 4 seconds
Sync interval: 1 days"

client_start 0

client_start 1
send "A\n"
send "[holl "DejaGnu test suite"]\n"
simple_expect "LysKOM" "connected"
send "1012 80 0 { }\n"
simple_expect "=1012"

client_start 2
send "A\n"
send "[holl "DejaGnu test suite"]\n"
simple_expect "LysKOM" "connected"
send "1013 80 0 { }\n"
simple_expect "=1013"
send "1014 0 5 [holl "gazonk"]\n"
simple_expect "=1014"

lyskomd_expect "Client 3 from $any* has been idle too long\.  Killing it\."
client_death 2
sleep 10
talk_to client 0
send "A3Hfoo\n"
simple_expect "LysKOM"
talk_to client 1
send "1015 35 \n"
simple_expect "=1015 $any_time"

system "kill -TERM $lyskomd_pid"
lyskomd_death "" signal
client_death 0
client_death 1
