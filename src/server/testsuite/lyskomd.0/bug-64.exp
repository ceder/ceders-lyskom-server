# Test suite for lyskomd.
# Copyright (C) 2004  Lysator Academic Computer Association.
#
# This file is part of the LysKOM server.
# 
# LysKOM is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by 
# the Free Software Foundation; either version 1, or (at your option) 
# any later version.
# 
# LysKOM is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details.
# 
# You should have received a copy of the GNU General Public License
# along with LysKOM; see the file COPYING.  If not, write to
# Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
# or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
# MA 02139, USA.
#
# Please report bugs at http://bugzilla.lysator.liu.se/. 

# Increase test coverage of aux-item-def-parse.y.

# Test the output specified by the -d flag.
lyskomd_start "lyskomd.0/bug-64.aux" "" "" "-d -f config/lyskomd-config" "" {} 1 0 5 0 0 {} {
    "Parsing definition of aux-item 1 \\(content-type\\)"
    "Parsing definition of aux-item 2 \\(fast-reply\\)"
    "Parsing definition of aux-item 3 \\(cross-reference\\)"
    "Parsing definition of aux-item 4 \\(no-comments\\)"
    "Parsing definition of aux-item 5 \\(personal-comment\\)"
    "Parsing definition of aux-item 7 \\(read-confirm\\)"
    "Parsing definition of aux-item 8 \\(redirect\\)"
    "Parsing definition of aux-item 9 \\(x-face\\)"
}

system "kill -TERM $lyskomd_pid"
lyskomd_death "" signal

# Test various fatal error messages.
lyskomd_fail_start \
    [list \
	 "<<<[pwd]/lyskomd.0/bug-64-2.aux: 8: invalid field name: xyzzy" \
	 "<<<[pwd]/lyskomd.0/bug-64-2.aux: 13: invalid type: expected boolean, got number" \
	 "<<<[pwd]/lyskomd.0/bug-64-2.aux: 14: invalid type: expected number, got boolean" \
	 "<<<[pwd]/lyskomd.0/bug-64-2.aux: 15: invalid type: expected boolean, got string" \
	 "<<<[pwd]/lyskomd.0/bug-64-2.aux: 16: invalid type: expected boolean, got identifier" \
	 "<<<[pwd]/lyskomd.0/bug-64-2.aux: 17: invalid type: expected identifier, got string" \
	 "<<<[pwd]/lyskomd.0/bug-64-2.aux: 18: undefined function: xyzzy" \
	 "<<<[pwd]/lyskomd.0/bug-64-2.aux: 19: undefined function: gazonk" \
	 "<<<[pwd]/lyskomd.0/bug-64-2.aux: 24: invalid type: expected identifier or string, got boolean" \
	 "Errors reading aux-item definition file" \
	] "lyskomd.0/bug-64-2.aux" "" "" "" {} {"Bug 1599" 0 31 0 2}

# Test the fatal error message when no aux-item definition file exists.
lyskomd_fail_start \
    [list \
	 "<<<[pwd]/lyskomd.0/nosuchfile.aux: No such file or directory" \
	 "Unable to open aux-item definition file" \
	] "lyskomd.0/nosuchfile.aux" "" "" "" {} {"Bug 1599" 0 25 0 0}
