# Test suite for lyskomd.
# Copyright (C) 2001-2003  Lysator Academic Computer Association.
#
# This file is part of the LysKOM server.
# 
# LysKOM is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by 
# the Free Software Foundation; either version 1, or (at your option) 
# any later version.
# 
# LysKOM is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details.
# 
# You should have received a copy of the GNU General Public License
# along with LysKOM; see the file COPYING.  If not, write to
# Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
# or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
# MA 02139, USA.
#
# Please report bugs at http://bugzilla.lysator.liu.se/. 



# Check that text, person and conference numbers are never ever
# reused, not even when the server crashes.

obtain_lock

lyskomd_start

# Create person, a conference and two texts.
client_start 0
talk_to client 0
send "A3Hfoo\n"
simple_expect "LysKOM"
send "1000 89 [holl "person before"] [holl "pb"] 00000000 0 { }\n"
simple_expect "=1000 6"
send "1001 62 6 [holl "pb"] 0\n"
simple_expect ":2 9 6 1"
simple_expect "=1001"
send "1002 88 [holl "conf b e f o r e"] 00001000 0 { }\n"
simple_expect "=1002 7"
send "1003 86 [holl "text 1 before"] 1 { 0 7 } 0 { }\n"
simple_expect "=1003 1"
send "1004 86 [holl "text 2 before"] 1 { 0 7 } 0 { }\n"
simple_expect "=1004 2"

kill_lyskomd
client_death 0

dbck_run

lyskomd_start "" "" "" "" "" {
    "WARN: Texts 1 - 2 were lost\\."
    "WARN: Confs 6 - 7 were lost\\."
} 0 1

# Create person, a conference and two texts.
client_start 0
talk_to client 0
send "A3Hfoo\n"
simple_expect "LysKOM"
send "1005 89 [holl "person after"] [holl "pa"] 00000000 0 { }\n"
extracting_expect "=1005 (6|8)" pa 1
if {$pa == 8} {
    pass "Person number retained"
} else {
    fail "Person number retained"
}
send "1006 62 $pa [holl "pa"] 0\n"
simple_expect ":2 9 $pa 1"
simple_expect "=1006"
send "1007 88 [holl "conf after"] 00001000 0 { }\n"
extracting_expect "=1007 (7|9)" ca 1
if {$ca == 9} {
    pass "Conference number retained"
} else {
    fail "Conference number retained"
}
send "1008 86 [holl "text 1 after"] 1 { 0 $ca } 0 { }\n"
good_bad_expect "=1008 3" "=1"
send "1009 86 [holl "text 2 after"] 1 { 0 $ca } 0 { }\n"
good_bad_expect "=1009 4" "=2"

system "kill -TERM $lyskomd_pid"

lyskomd_death {} signal
client_death 0

release_lock
