#!/bin/sh
# Copyright (C) 1998, 2002-2003  Lysator Academic Computer Association.
# 
# This file is part of the LysKOM server.
# 
# LysKOM is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by 
# the Free Software Foundation; either version 1, or (at your option) 
# any later version.
# 
# LysKOM is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details.
# 
# You should have received a copy of the GNU General Public License
# along with LysKOM; see the file COPYING.  If not, write to
# Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
# or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
# MA 02139, USA.
# 
# Please report bugs at http://bugzilla.lysator.liu.se/. 

# Scan all test cases, and emit information about the coverage.
# The first column in the output is the Protocol A call number.
# If a line contains more than one column the second column is the
# error number; otherwise the call succeeded.
#
# This script relies on the formatting of the test cases in obvious
# ways.  See the code below for more information.
cat *.exp | sed -n -e 's/^send "\([0-9][0-9]* [0-9][0-9]*\).*\\n"$/\1/p' \
	-e 's/^simple_expect "=\([0-9][0-9]*\).*".*/= \1/p' \
	-e 's/^simple_expect "%\([0-9][0-9]*\) \([0-9][0-9]*\) \([0-9][0-9]*\)".*/% \1 \2 \3/p' \
| awk '$1 == "=" { ok[call] = ok[call] + 1; next }
$1 == "%" { err[call " " $3] = err[call " " $3] + 1; next }
{ ref = $1; call = $2; next }
END { for (k in ok) { print k } 
      for (k in err) { print k } 
}' \
| sort +0n +1n
